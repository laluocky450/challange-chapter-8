const AuthenticationController = require("./AuthenticationController");
const { User } = require("../models");
const { EmailNotRegisteredError, InsufficientAccessError, RecordNotFoundError, WrongPasswordError } = require("../errors");
const { JWT_SIGNATURE_KEY } = require("../../config/application");
const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");

describe("AuthenticationController", () => {
//   describe("#authorize", () => {
//     it("should be authorize", async () => {

//         const mockRequest = {
//             token : {header: {authorize : () => {
//                 return;
//             }}  },
//             payload : jest.fn().mockReturnThis(),  
//         };
//         const user = new User(mockRequest.token, mockRequest.payload);

//         const mockResponse = {
//             status: jest.fn().mockReturnThis(),
//             json: jest.fn().mockReturnThis(),
//             };

//         const err = new InsufficientAccessError();
//         const error = {
//             error : {
//                 name: err.name,
//                 message: err.message,
//                 details: {
//                   role: string,
//                   reason: string,
//               },
//             },
//         };

//         const authenticationController = new AuthenticationController("userModel",
//         "roleModel",
//         "bcrypt",
//         "jwt");
//         const result = authenticationController.authorize(mockRequest, mockResponse);

//         expect(result.user).toMatchObject(user);
//         expect(result.status).toHaveBeenCall(401);
//         expect(result.json).toHaveBeenCall(error);

//     })
// })

// describe("#handleRegister", () => {
//     it("should register user", async () => {
//       const payloadUser = {
//         name: "oki",
//         email: "loki@gmail.com",
//         password: "loki1234",
//       };

//       const mockRequest = {
//         body: {
//           payloadUser,
//         },
//       };

//       const mockResponse = {
//         status: jest.fn().mockReturnThis(),
//         json: jest.fn().mockReturnThis(),
//       };

//       const mockNext = {};

//       const mockUser = new User({
//         payloadUser,
//       });
//       const mockUserModel = {};
//       let existingUser = (mockUserModel.findOne = jest
//         .fn()
//         .mockReturnValue(mockUser));
//       const app = new AuthenticationController({
//         userModel: mockUserModel,
//       });
//       const result = await app.handleRegister(
//         mockRequest,
//         mockResponse,
//         mockNext
//       );

//       expect(result).toEqual();
//     });
//   });

  describe("#decodeToken", () => {
    it("should decode token", async () => {
      const user = {
        id: 1,
        name: "oki",
        email: "loki@gmail.com",
        image: "oki.jpg",
      };
  
      const role = {
        id: 1,
        name: "ADMIN",
      };
  
      const mockUser = user;
      const mockRole = role;
  
      const token = jsonwebtoken.sign(
        {
          id: mockUser.id,
          name: mockUser.name,
          email: mockUser.email,
          image: mockUser.image,
          role: {
            id: mockRole.id,
            name: mockRole.name,
          },
        },
        JWT_SIGNATURE_KEY
      );
  
      const decoded = jsonwebtoken.verify(token, JWT_SIGNATURE_KEY);
      const app = new AuthenticationController({
        jwt: jsonwebtoken,
      });
      const result = await app.decodeToken(token);
  
      expect(result).toEqual(decoded);
    });
  });
  
  describe("#encryptPassword", () => {
    it("should encrypt the password", async () => {
      const password = "loki1234";
      const encrypt = bcrypt.hashSync(password, 10);
  
      const app = new AuthenticationController({
        jwt: jsonwebtoken,
        bcrypt: bcrypt,
      });
      const result = await app.encryptPassword(password);
  
      expect(result.slice(0, -53)).toEqual(encrypt.slice(0, -53));
    });
  });
  
  describe("#verifyPassword", () => {
    it("should verify password and encrypted one", async () => {
      const password = "loki1234";
  
      const encryptedPassword = bcrypt.hashSync(password, 0);
  
      const verif = bcrypt.compareSync(password, encryptedPassword);
  
      const app = new AuthenticationController({
        jwt: jsonwebtoken,
        bcrypt: bcrypt,
      });
      const result = await app.verifyPassword(password, encryptedPassword);
  
      expect(result).toEqual(verif);
    });
  });
  
})